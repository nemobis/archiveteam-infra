## archiveteam-infra

Ready to save the web, one archiveteam project at a time? Then this project is for you!

(If you have no idea what we're talking about, see [ArchiveTeam Warrior](https://www.archiveteam.org/index.php?title=ArchiveTeam_Warrior).)

## Installation

Get a DigitalOcean API Token. https://cloud.digitalocean.com/account/api/tokens/new

Upload your ssh key to DigitalOcean. You can do that here https://cloud.digitalocean.com/account/security Make a copy of the ssh fingerprint as you need it later.

Copy `terraform.tfvars.dist` to `terraform.tfvars`, filling out the wanted values.

Now run `terraform apply`. Once it's done, you're helping! Congratz.

### LICENSE: MIT (2018) DIGGAN
